<%--
  Created by IntelliJ IDEA.
  User: Marcin
  Date: 2019-04-06
  Time: 12:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
          integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
</head>
<body>

<p>ID: ${vProduct.getId()}</p>
<p>Name: ${vProduct.getName()}</p>
<p>Description: ${vProduct.getDescription()}</p>
<p>Category: ${vProduct.getCategory()}</p>
<p>Price: ${vProduct.getPrice()} PLN</p>

<form action="${pageContext.request.contextPath}/addToCart" method="post">
    <p>
        <span style="font-size: 144%; "><i class="fas fa-shopping-cart"></i></span>
    <th><input type="submit" name="addProdToCart" value="Add to Shopping Cart"/></th>
    Quantity:
    <th><input type="number" name="quantity" value="1" style="width: 75px;"/></th>
    <input type="hidden" name="productID" value=${vProduct.getId()}>
    </p>
</form>

<p>
    <a href="${pageContext.request.contextPath}/homepage"> Return to Main Page </a>
</p>

</body>
</html>
