<%@ page import="java.time.LocalDate" %>
<html>
<body>
<%! int counter = 0; %>

<jsp:include page="header.jsp"/>

<h2>Hello World!</h2>

Today: <%=LocalDate.now().toString()%>

<br><br>
Next 10 days:
<br>
<% LocalDate today = LocalDate.now();
    for (int i = 0; i < 10; i++) {
        out.println(today.plusDays(i).toString() + "\n");
%>
<br>
<%
    }
%>

<br>

Page visit counter:
<%= counter++%>

<p> ${name} </p>

<jsp:include page="footer.jsp"/>

</body>
</html>
