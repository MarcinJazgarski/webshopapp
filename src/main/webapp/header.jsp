<%@ page import="model.User" %>
<%@ page import="model.UserRoles" %><%--
  Created by IntelliJ IDEA.
  User: Marcin
  Date: 2019-03-30
  Time: 12:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Menu</title>
    <!-- stylesheets from bootstrap.com https://getbootstrap.com/docs/4.3/getting-started/introduction/
   Copy-paste the stylesheet <link> into your <head> before all other stylesheets to load our CSS-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="RESOURCES/Homepage.css">
</head>

<body>

<div class="row">
    <div class="col-sm" style="max-width: fit-content">
        <!-- column area -->
        <p>
            <%-- check if user logged in (session active) --%>
            <% if (session.getAttribute("loggedUser") != null) {
            %> Hi  <%out.print(((User) session.getAttribute("loggedUser")).getLogin());%>!
        </p>
    </div>
    <div class="col-sm" style="max-width: fit-content">
        <!-- column area -->
        <p>
            <a href="${pageContext.request.contextPath}/logout"> Logout </a>
            <%} else {%>
            <a href="${pageContext.request.contextPath}/login"> Login </a>
        </p>
    </div>

    <div class="col-sm" style="max-width: fit-content">
        <!-- column area -->
        <a href="${pageContext.request.contextPath}/register_user"> New User </a>
        <%
            }
            ;%>
        </p>
    </div>

    <div class="col-sm" style="max-width: fit-content">
        <!-- column area -->
        <p>
            <a href="${pageContext.request.contextPath}/homepage"> Main Page </a>
        </p>
    </div>

    <div class="col-sm" style="max-width: fit-content">
        <!-- column area -->
        <p>
            <a href="${pageContext.request.contextPath}/addToCart"> Cart </a>
        </p>
    </div>

    <div class="col-sm" style="max-width: fit-content">
        <!-- column area -->
        <%-- check if user logged in (session active), add product link should display only if user with admin role logged in --%>
        <% if (session.getAttribute("loggedUser") != null && ((User) session.getAttribute("loggedUser")).getRole().equals(UserRoles.ADMIN)) {
        %> <p>
        <a href="${pageContext.request.contextPath}/addproduct"> Add Product </a>
    </p>
        <% } %>
    </div>

</div>


<form action="${pageContext.request.contextPath}/homepage" method="get">
    <p>Filter by Product Category:
        <input type="text" name="category"/>
        <input type="submit" name="submit" value="filter"/>
    </p>
</form>

<!-- scripts from bootstrap.com https://getbootstrap.com/docs/4.3/getting-started/introduction/
Place the following <script>s near the end of your pages, right before the closing </body> tag, to enable them. jQuery must come first, then Popper.js, and then our JavaScript plugins-->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</body>

</html>
