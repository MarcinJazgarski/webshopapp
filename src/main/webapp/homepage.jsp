<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Marcin
  Date: 2019-03-30
  Time: 14:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>

    <!-- stylesheets from bootstrap.com https://getbootstrap.com/docs/4.3/getting-started/introduction/
    Copy-paste the stylesheet <link> into your <head> before all other stylesheets to load our CSS-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="RESOURCES/Homepage.css">

</head>

<body>
<!--<body background="RESOURCES/WebpageBackgroundPHOTOGRAPHY18.jpg">-->

<div class="container">

    <jsp:include page="header.jsp"/>

    <h2>List of available products:</h2>

    <table class="table table-hover">
        <thead>
        <tr>
            <th class="colA" scope="col">link</th>
            <th class="colB" scope="col">Product Name</th>
            <th scope="col">Description</th>
            <th scope="col">Category</th>
            <th scope="col">Price</th>
        </tr>
        </thead>

        <c:forEach items="${prods}" var="product">

        <tbody>
        <tr>
            <th scope="row">
                <a href="${pageContext.request.contextPath}/viewProduct?id=${product.getId()}"> LINK </a></th>
            <td><b>${product.getName()}</b></td>
            <td>${product.getDescription()}</td>
            <td>${product.getCategory()}</td>
            <td><b>${product.getPrice()} PLN</b></td>

            </c:forEach>

        </tbody>
    </table>


</div>

<!-- scripts from bootstrap.com https://getbootstrap.com/docs/4.3/getting-started/introduction/
Place the following <script>s near the end of your pages, right before the closing </body> tag, to enable them. jQuery must come first, then Popper.js, and then our JavaScript plugins-->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>

</body>
</html>
