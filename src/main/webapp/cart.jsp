<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="model.Cart" %>
<%@ page import="model.Product" %>
<%@ page import="java.util.Map" %>
<%--
  Created by IntelliJ IDEA.
  User: Marcin
  Date: 2019-04-13
  Time: 11:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" type="text/css" href="RESOURCES/Cart.css">
</head>
<body>

<p>
    <%--  --%>
    <% Cart cart = (Cart) session.getAttribute("cart");
        //out.print(cart);
        Map<Product, Integer> products = cart.getProducts();
        for (Map.Entry<Product, Integer> e : products.entrySet()) {
            out.print(e.getKey());
    %>
    <br>
    <%
        out.print(" order quantity: ");
    %> <b><% out.print(e.getValue()); %></b> <%
    long price = 0;
    price = ((e.getKey().getPrice() * Long.valueOf(e.getValue())));
    out.print(", extended net price: ");
%> <b><% out.print(price + " PLN"); %></b> <%
%>
    <br> <br>
    <%
        } %>
    <b>
        Total Price:
        <% long totalPrice = 0;
            for (Map.Entry<Product, Integer> e : products.entrySet()) {
                totalPrice = totalPrice + (e.getKey().getPrice() * Long.valueOf(e.getValue()));
            }
            out.print(totalPrice);
        %> PLN</b>

</p>
<p>
    <a href="${pageContext.request.contextPath}/homepage"> Back to Main Page </a>
</p>


</body>
</html>
