package pl.sda.jazgarski.filters;

import model.User;
import model.UserRoles;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(servletNames = "ProductAddServlet")
public class AdminSecurityFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        //tutaj przetwarzanie request przed przekazaniem do servletu

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        User loggedUser = (User) req.getSession().getAttribute("loggedUser");
        if (loggedUser != null) {
            if (loggedUser.getRole().equals(UserRoles.ADMIN)) {
                //można przejść dalej
                filterChain.doFilter(request, response); //tutaj przetwarzanie response po odpowiedzi z servletu
            } else {//nie można iść dalej}
                req.setAttribute("nonAdmin", "nonAdmin");
                res.sendRedirect("/homepage");
            }
        } else {//nie można iść dalej
            req.setAttribute("nonAdmin", "nonAdmin");
            res.sendRedirect("/homepage");
        }
    }

    @Override
    public void destroy() {

    }
}


/*
 * stworzyć nowy filter AdminSecurityFilter implements Filter
 * Podpiąc go pod AddProductServlet
 * Sprawdzać w nim czy zalogowany użytkownik ma role admin
 * Add product link widoczny tylko dla admina*/


/*
 * Na stronie głownej gdzie wyświetlane są produkty dodać linki które prowadzą na stronę każdego z nich przez ViewProductServlet
 *
 * W servlecie ViewProduct wysłać do użytkownika ciasteczko lastView z id ostatnio oglądanego produktu
 *
 * W głównym servlecie sprawdzić czy użytkownik wyslał to ciasteczko
 *   jeśli tak wyśliwtlić mu super reklame ostatio oglądanego produktu
 *   jeśli nie nie wyświetlać nic*/