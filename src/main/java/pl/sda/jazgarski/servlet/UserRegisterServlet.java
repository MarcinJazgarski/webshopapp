package pl.sda.jazgarski.servlet;

import model.DAOUserDb;
import model.UserRoles;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(urlPatterns = "/register_user")
public class UserRegisterServlet extends HttpServlet {

    private static final long serialVersionUID = 3955675334489124359L;
    private DAOUserDb userDb;

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        request
                .getRequestDispatcher("registerUser.jsp")
                .forward(request, response);
    }

    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {

        final String email = request.getParameter("userEmail");
        final String login = request.getParameter("userLogin");
        final String password = request.getParameter("userPassword");

        //check if email and login provided do not exist in userdatabase
        final boolean newUserValidationResult = userDb.getAllUsers().stream().noneMatch(user -> user.getLogin().equalsIgnoreCase(login) && user.getEmail().equalsIgnoreCase(email));

        if (newUserValidationResult) {
            userDb.createNewUser(email, login, password, UserRoles.USER);
            PrintWriter out = response.getWriter();
            out.print("user added");
            response.setContentType("text/html");
            String link = String.format("<br><br> <a href=\"%s\">back to HOMEPAGE</a>", request.getRequestURI().replace("/register_user", "/homepage"));
            response.getWriter().println(link);
        } else {
            response.getWriter().print("user NOT added - login or email already exists");
            response.setContentType("text/html");
            String link = String.format("<br><br> <a href=\"%s\">Try Again</a>", request.getRequestURI());
            response.getWriter().println(link);
            String link1 = String.format("<br><br> <a href=\"%s\">back to HOMEPAGE</a>", request.getRequestURI().replace("/register_user", "/homepage"));
            response.getWriter().println(link1);
        }
    }

    @Override
    public void init() throws ServletException {
        userDb = DAOUserDb.getInstance();
        super.init();
    }
}
