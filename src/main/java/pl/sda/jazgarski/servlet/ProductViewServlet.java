package pl.sda.jazgarski.servlet;

import model.DAOProductDb;
import model.Product;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

//adnotacja przypisuje servlet do adresu (ten servlet będzie czekał na przetworzenie zapytań z tego adresu)
@WebServlet(urlPatterns = "/viewProduct", name = "productViewServlet")
public class ProductViewServlet extends HttpServlet {

    private static final long serialVersionUID = -4873829925002598945L;
    private DAOProductDb productDb;

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {

        final Long prodId = Long.valueOf(request.getParameter("id"));

        final Optional<Product> productById = this.productDb.getProductById(prodId);
        if (productById.isPresent()) {
            request.setAttribute("vProduct", productById.get());
            Cookie cookie = new Cookie("lastViewedProduct", prodId.toString());
            response.addCookie(cookie);
        }
        request
                .getRequestDispatcher("viewProduct.jsp")
                .forward(request, response);
    }

    @Override
    public void init() throws ServletException {
        this.productDb = DAOProductDb.getInstance();
        super.init();
    }
}
