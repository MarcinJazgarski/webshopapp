package pl.sda.jazgarski.servlet;

import model.DAOProductDb;
import model.Product;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

//adnotacja przypisuje servlet do adresu (ten servlet będzie czekał na przetworzenie zapytań z tego adresu)
@WebServlet(urlPatterns = "/homepage")
public class HomepageServlet extends HttpServlet {

    private DAOProductDb productDb;

    @Override
    protected void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {

        //example url inquiry: localhost:8888/homepage?category=Hunter
        String categ = httpServletRequest.getParameter("category");
        List<Product> products = productDb.getAllProducts();
        if (categ != null) {
            products = products.stream().filter(product -> product.getCategory().equals(categ)).collect(Collectors.toList());
        }
        httpServletRequest.setAttribute("prods", products);
        httpServletRequest.getRequestDispatcher("/homepage.jsp").forward(httpServletRequest, httpServletResponse);

        /*request.getCookies
        sprawdź czy cookies nie są null
        for each na tablicy cookiesów
        sprawdź czy któreś cookie na nazwę którą szukam "lastViewedProduct"
        jeśli ma to pobierz produkt o wartości tego cookie z bazy i wrzuć jako atrybut requesta
        wyświetl produkt na frontendzie
        * */
    }

    //init należy traktować jak konstruktor servletu
    @Override
    public void init() throws ServletException {
        productDb = DAOProductDb.getInstance();
        super.init();
    }
}