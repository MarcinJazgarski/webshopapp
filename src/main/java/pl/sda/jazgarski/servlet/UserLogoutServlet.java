package pl.sda.jazgarski.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/logout")
public class UserLogoutServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.getSession().invalidate();
        response.getWriter().print("logged out");
        response.setContentType("text/html");
        String link = String.format("<br><br> <a href=\"%s\">back to HOMEPAGE</a>", request.getRequestURI().replace("/logout", "/homepage"));
        response.getWriter().println(link);

    }
}
