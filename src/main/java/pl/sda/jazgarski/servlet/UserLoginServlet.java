package pl.sda.jazgarski.servlet;

import model.DAOUserDb;
import model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet(urlPatterns = "/login")
public class UserLoginServlet extends HttpServlet {

    private DAOUserDb userDb;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request
                .getRequestDispatcher("userLogin.jsp")
                .forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String loginTypedIn = request.getParameter("userLogin");
        String passwordTypedIn = request.getParameter("userPassword");

        Optional<User> userOptional = userDb.getAllUsers().stream().filter(user -> user.getLogin().equals(loginTypedIn)).findFirst();
        if (userOptional.isPresent()) {

            if (userOptional.get().getPassword().equals(passwordTypedIn)) {
                // login successful
                request.getSession().setAttribute("loggedUser", userOptional.get());
                response.getWriter().print("login OK");
                response.setContentType("text/html");
                String link = String.format("<br><br> <a href=\"%s\">back to HOMEPAGE</a>", request.getRequestURI().replace("/login", "/homepage"));
                response.getWriter().println(link);

            } else {
                response.getWriter().print("incorrect password");
                response.setContentType("text/html");
                String link = String.format("<br><br> <a href=\"%s\">Try Again</a>", request.getRequestURI());
                response.getWriter().println(link);
                String link1 = String.format("<br><br> <a href=\"%s\">back to HOMEPAGE</a>", request.getRequestURI().replace("/login", "/homepage"));
                response.getWriter().println(link1);
            }

        } else {
            response.getWriter().print("login does not exist");
            response.setContentType("text/html");
            String link = String.format("<br><br> <a href=\"%s\">Try Again</a>", request.getRequestURI());
            response.getWriter().println(link);
            String link1 = String.format("<br><br> <a href=\"%s\">back to HOMEPAGE</a>", request.getRequestURI().replace("/login", "/homepage"));
            response.getWriter().println(link1);
        }
    }

    /*do login servlet oraz register servlet dodaj logowanie
    logowanie dodaj poprzez wpisanie obiektu user do sesji
    dodaj logout servlet
    servlet pod metodą get powinien inwalidować sesję
    */

    /*
     * w header.jsp za pomoca skrypletu sprawdz czy ktos jest zalogowany
     * jeśli jest zalogowany wyswietl mu wiadomosc hello, %userName% oraz wyświetl link do wylogowania
     * Jeśli nie jest zalogowany wyświetl link do formatki logowania
     * */

    @Override
    public void init() throws ServletException {
        userDb = DAOUserDb.getInstance();
        super.init();
    }
}

