package pl.sda.jazgarski.servlet;

import model.DAOProductDb;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

//adnotacja przypisuje servlet do adresu (ten servlet będzie czekał na przetworzenie zapytań z tego adresu)
@WebServlet(urlPatterns = "/addproduct", name = "ProductAddServlet")
public class ProductAddServlet extends HttpServlet {

    private static final long serialVersionUID = -1527141136296040171L;
    private DAOProductDb productDb;

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        request
                .getRequestDispatcher("addProduct.jsp")
                .forward(request, response);
    }

    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {

        final String productName = request.getParameter("prodName");
        final String productDescription = request.getParameter("prodDescription");
        final String productCategory = request.getParameter("prodCategory");
        final Long productPrice = Long.valueOf(request.getParameter("prodPrice"));

        final long newProductId = this.productDb.createNewProduct(productName, productDescription, productCategory, productPrice);

        //Servlet POST addProduct powinien przekierowac na nowy servlet za pomoca + response.sendRedirect("/viewProduct?id=idnowego")
        response.sendRedirect("/viewProduct?id=" + newProductId);
    }

    //init należy traktować jak konstruktor servletu
    @Override
    public void init() throws ServletException {
        this.productDb = DAOProductDb.getInstance();
        super.init();
    }
}



