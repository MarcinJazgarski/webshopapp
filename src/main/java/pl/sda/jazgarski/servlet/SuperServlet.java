package pl.sda.jazgarski.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/home")
public class SuperServlet extends HttpServlet {
    private static final long serialVersionUID = 8201331134510845592L;

    @Override
    protected void doGet(final HttpServletRequest httpServletRequest, final HttpServletResponse httpServletResponse)
            throws ServletException, IOException {

//        PrintWriter writer = httpServletResponse.getWriter();
//        writer.print("Hi world");

//        httpServletRequest.setAttribute("name", "Marcin");

        httpServletRequest
                .getRequestDispatcher("index.jsp")
                .forward(httpServletRequest, httpServletResponse);
    }
}
