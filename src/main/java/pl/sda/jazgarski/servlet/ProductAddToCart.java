package pl.sda.jazgarski.servlet;

import model.Cart;
import model.DAOProductDb;
import model.Product;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

@WebServlet(urlPatterns = "/addToCart", name = "ProductAddToCartServlet")
public class ProductAddToCart extends HttpServlet {

    private DAOProductDb productDb;


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Cart cart = (Cart) request.getSession().getAttribute("cart");
        if (cart == null) {
            cart = new Cart();
        }
        request.getSession().setAttribute("cart", cart);
        request.getRequestDispatcher("/cart.jsp")
                .forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        long productID = Long.parseLong(request.getParameter("productID"));
        int quantity = Integer.parseInt(request.getParameter("quantity"));

        Optional<Product> optionalProduct = productDb.getProductById(productID);

        Cart cart = (Cart) request.getSession().getAttribute("cart");
        if (cart == null) {
            cart = new Cart();
        }

        if (optionalProduct.isPresent()) {
            cart.updateQuantity(optionalProduct.get(), quantity);
            request.getSession().setAttribute("cart", cart);
        }

        request.getRequestDispatcher("/cart.jsp")
                .forward(request, response);
    }

    @Override
    public void init() throws ServletException {
        productDb = DAOProductDb.getInstance();
        super.init();
    }
}
