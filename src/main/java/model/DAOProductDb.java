package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class DAOProductDb {

    //singleton
    private static final DAOProductDb SINGLE_INSTANCE = new DAOProductDb();
    private static long currentProductId = 4L;
    private final List<Product> productList;

    private DAOProductDb() {
        this.productList = new ArrayList<>();
        this.productList.add(new Product(1L, "pen1", "blue pen1", "pens", 3L));
        this.productList.add(new Product(2L, "pen2", "blue pen2", "pens", 5L));
        this.productList.add(new Product(3L, "pen3", "blue pen3", "category3", 8L));
    }

    //singleton
    public static DAOProductDb getInstance() {
        return DAOProductDb.SINGLE_INSTANCE;
    }


    public List<Product> getAllProducts() {
        return this.productList;
    }

    public Optional<Product> getProductById(final long id) {
        final Optional<Product> product = this.productList.stream().filter(p -> p.getId() == id).findAny();
        return product;
    }

    public long createNewProduct(final String name, final String description, final String category, final Long price) {
        final Product product = new Product(DAOProductDb.currentProductId + 1, name, description, category, price);
        this.productList.add(product);
        DAOProductDb.currentProductId += 1;
        return product.getId();
    }

}
