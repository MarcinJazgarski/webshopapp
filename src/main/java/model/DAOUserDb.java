package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class DAOUserDb {

    //singleton
    private static final DAOUserDb SINGLE_INSTANCE = new DAOUserDb();
    private final List<User> userList;

    private DAOUserDb() {
        this.userList = new ArrayList<>();
        this.userList.add(new User("admin@admin.com", "admin", "admin", UserRoles.ADMIN));
    }

    //singleton
    public static DAOUserDb getInstance() {
        return DAOUserDb.SINGLE_INSTANCE;
    }

    public List<User> getAllUsers() {
        return this.userList;
    }

    public Optional<User> getUserByLogin(final String login) {
        final Optional<User> user = this.userList.stream().filter(u -> u.getLogin() == login).findAny();
        return user;
    }

    //wskazane hashowanie hasła RSA256 lub RSA512
    public void createNewUser(final String email, final String login, final String password, final UserRoles role) {
        final User user = new User(email, login, password, role);
        this.userList.add(user);
    }
}
