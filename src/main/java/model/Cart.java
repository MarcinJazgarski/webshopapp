package model;

import java.util.LinkedHashMap;
import java.util.Map;

public class Cart {

    private final Map<Product, Integer> products;

    public Cart() {
        products = new LinkedHashMap<>();
    }

    public Map<Product, Integer> getProducts() {
        return products;
    }

    public void updateQuantity(final Product product, final Integer quantity) {
        final Integer oldQuantity = products.getOrDefault(product, 0);
        if (oldQuantity + quantity > 0) {
            products.put(product, oldQuantity + quantity);
        } else {
            products.remove(product);
        }
    }

    public void clear() {
        products.clear();
    }

    @Override
    public String toString() {
        return "Cart{" +
                "products=" + products +
                '}';
    }
}
